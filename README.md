# Experiment Express

> Experiment from 2016

This is a small project that intends to test Webpack 1, Jade and RamdaJS in an Express application.

It is a app that display mp3 links from a list of podcasts

## Results 📝

- OMG, Webpack is a mess and the documentation is awful. I'm not sure I
  understood everything. I don't know how to ouput CommonJS files... The
  bundling system with Webpack still doesn't work.
- I learn a lot in functionnal programming with [Ramda](https://ramdajs.com/),
  especially the usage of `pipe` and `compose`.
- I only know JS promises from JQuery. I use another library for this project.
  The [Q module](https://github.com/kriskowal/q) teach me how to use `defer`.

#### Console output:
![Screenshot_2021-02-06_073904](/uploads/df08383db405f3c856b4d116bcd3ea83/Screenshot_2021-02-06_073904.png)

#### Browser output on `localhost:3000`:
![Screenshot_2021-02-06_Ynote_hk_-_dl-podcasts](/uploads/50237c5179fc52bf03e4b17b3be2f4af/Screenshot_2021-02-06_Ynote_hk_-_dl-podcasts.png)

## Requirements

- Node 4.8.0 (This version is needed for compatibility with Gulp 3)

## Usage

Edit the `conf/podcasts.json`

Install the dependencies:
```
npm i
```

Build the app:
```
npm run build
```

Launch the server
```
npm start
```
